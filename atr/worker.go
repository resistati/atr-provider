package atr

import (
	"context"
	"os"
	"os/signal"
	"time"

	"github.com/getsentry/sentry-go"
	"github.com/rs/zerolog"

	"gitlab.com/resistati/atr-provider/cache"
	"gitlab.com/resistati/atr-provider/config"
	"gitlab.com/resistati/atr-provider/redis"
	"gitlab.com/resistati/atr-provider/tr"
)

type Worker struct {
	redis     *redis.RedisClient
	conf      *config.Config
	logger    *zerolog.Logger
	atrSetter cache.AtrSetter
}

func NewWorker(
	r *redis.RedisClient,
	conf *config.Config,
	logger *zerolog.Logger,
	atrSetter cache.AtrSetter,
) Worker {
	return Worker{r, conf, logger, atrSetter}
}

func (c *Worker) Spawn(ctx context.Context) error {
	hub := sentry.GetHubFromContext(ctx)

	ticker := time.NewTicker(time.Duration(c.conf.AtrRecalculateIntervalSeconds) * time.Second)
	c.logger.Trace().Uint("seconds", c.conf.AtrRecalculateIntervalSeconds).Msg("initialized ticket")
	defer func() {
		c.logger.Trace().Msg("stopping ticker")
		ticker.Stop()
	}()

	c.logger.Trace().Msg("binding to interrupt signal")
	close := make(chan os.Signal, 1)
	signal.Notify(close, os.Interrupt)

	c.logger.Trace().Msg("starting the loop")
	for {
		select {
		case <-ticker.C:
			{
				c.logger.Trace().Msg("recalculating atr due to clock tick occurred")

				span := sentry.StartSpan(ctx, "calculate-atr", sentry.TransactionName("calculate-atr"))
				defer span.Finish()

				child := span.StartChild("get-candlesticks-from-redis")
				cs, err := c.redis.GetCandlesticks(ctx)
				if nil != err {
					defer child.Finish()
					c.logger.Err(err).Msg("unable to get all candlesticks from redis")
					hub.CaptureException(err)
					return err
				}
				c.logger.Debug().Int("len", len(cs)).Msg("got all candlesticks from redis")
				child.Finish()

				child = span.StartChild("calculate-candlesticks-true-ranges")
				trs := tr.FromCandlesticks(cs)
				c.logger.Debug().Msg("calculated true ranges")
				child.Status = sentry.SpanStatusOK
				child.Finish()

				child = span.StartChild("calculate-true-ranges-atr")
				atr := calculateFromTrs(trs)
				c.logger.Info().Float64("atr", atr).Msg("calculated atr")
				child.Status = sentry.SpanStatusOK
				child.Finish()

				c.logger.Trace().Msg("setting atr cache")
				if err := c.atrSetter.Set(span.Context(), atr); nil != err {
					c.logger.Err(err).Msg("failed to set cache with calculated atr value. returning from the loop")
					hub.CaptureException(err)
					return err
				}
				c.logger.Info().Msg("set cache with calculated atr value")

				span.Status = child.Status
				span.Finish()
			}
		case <-close:
			{
				c.logger.Info().Msg("interrupt signal received. returning from the loop")
				return nil
			}
		}
	}
}
