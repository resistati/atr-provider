package atr

import (
	"gitlab.com/resistati/atr-provider/rma"
)

func zeroOr(zeroable, or float64) float64 {
	if zeroable == 0 {
		return 0
	}

	return or
}

func calculateFromTrs(trs []float64) float64 {
	rma5 := rma.Rma(trs, rma.PeriodLength5)
	rma11 := rma.Rma(trs, rma.PeriodLength11)
	rma22 := rma.Rma(trs, rma.PeriodLength22)
	rma45 := rma.Rma(trs, rma.PeriodLength45)
	rma91 := rma.Rma(trs, rma.PeriodLength91)
	rma182 := rma.Rma(trs, rma.PeriodLength182)
	rma364 := rma.Rma(trs, rma.PeriodLength364)
	div := zeroOr(rma5, 1) +
		zeroOr(rma11, 1) +
		zeroOr(rma22, 2) +
		zeroOr(rma45, 3) +
		zeroOr(rma91, 5) +
		zeroOr(rma182, 8) +
		zeroOr(rma364, 13)
	out := (rma364*13 + rma182*8 + rma91*5 + rma45*3 + rma22*2 + rma11*1 + rma5*1) / div

	return out
}
