package cache

import (
	"context"
	"errors"

	"github.com/getsentry/sentry-go"
	"github.com/rs/zerolog"

	"gitlab.com/resistati/atr-provider/redis"
)

var (
	ErrNotSet error = errors.New("atr cache is not set yet")
)

const atrCacheKey = "atr"

type AtrGetter interface {
	Get(context.Context) (float64, error)
}

type AtrSetter interface {
	Set(context.Context, float64) error
}

type AtrSetterGetter interface {
	AtrSetter
	AtrGetter
}

type AtrCache struct {
	ctx         context.Context
	logger      *zerolog.Logger
	redisClient *redis.RedisClient
}

func NewAtrCache(
	ctx context.Context,
	logger *zerolog.Logger,
	redisClient *redis.RedisClient,
) AtrSetterGetter {
	return AtrCache{ctx, logger, redisClient}
}

func (a AtrCache) Get(ctx context.Context) (float64, error) {
	hub := sentry.GetHubFromContext(ctx)
	span := sentry.StartSpan(ctx, "read-atr-from-cache")
	defer span.Finish()

	child := span.StartChild("read-atr-from-redis")
	atr, err := a.redisClient.GetAtr(ctx)
	if nil != err {
		defer child.Finish()
		a.logger.Err(err).Msg("failed to read cached atr value")
		child.Status = sentry.SpanStatusInternalError
		hub.CaptureException(err)
		return 0, err
	}
	a.logger.Debug().Msg("read cached atr value")
	child.Status = sentry.SpanStatusOK
	child.Finish()
	return atr, nil
}

func (a AtrCache) Set(ctx context.Context, atr float64) (err error) {
	hub := sentry.GetHubFromContext(ctx)
	span := sentry.StartSpan(ctx, "set-atr-to-cache")
	defer span.Finish()

	child := span.StartChild("set_to_cache")
	if err = a.redisClient.SetAtr(ctx, atr); nil != err {
		defer child.Finish()
		a.logger.Err(err).Msg("unable to set atr value in cache")
		child.Status = sentry.SpanStatusInternalError
		hub.CaptureException(err)
		return
	}
	a.logger.Debug().Msg("successfully set atr value in cache")
	child.Status = sentry.SpanStatusOK
	child.Finish()

	return
}
