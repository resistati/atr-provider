package main

import (
	"context"
	"os"
	"os/signal"
	"time"

	"github.com/getsentry/sentry-go"
	"github.com/rs/zerolog"
	"golang.org/x/sync/errgroup"

	"gitlab.com/resistati/atr-provider/atr"
	"gitlab.com/resistati/atr-provider/binance"
	"gitlab.com/resistati/atr-provider/cache"
	"gitlab.com/resistati/atr-provider/config"
	"gitlab.com/resistati/atr-provider/http"
	"gitlab.com/resistati/atr-provider/redis"
)

func main() {
	zerolog.TimeFieldFormat = zerolog.TimeFormatUnix
	logger := zerolog.New(zerolog.ConsoleWriter{Out: os.Stdout, NoColor: false, TimeFormat: time.RFC3339}).With().Timestamp().Logger()

	ctx := context.Background()

	logger.Trace().Msg("loading configuration")
	configLogger := logger.With().Str("mod", "config").Logger()
	conf, err := config.Load(configLogger.WithContext(ctx))
	if nil != err {
		logger.Fatal().Err(err).Msg("unable to load configuration")
	}

	redisClient := redis.New(&conf.Redis)
	if err := redisClient.Connect(ctx); nil != err {
		logger.Fatal().Err(err).Msg("unable to connect to redis")
	}
	defer redisClient.Close()

	sentryHTTPSyncTransport := sentry.NewHTTPSyncTransport()
	sentryHTTPSyncTransport.Timeout = 3 * time.Second
	if err := sentry.Init(sentry.ClientOptions{
		Dsn:              conf.APM.DSN,
		Environment:      conf.APM.Env,
		Release:          conf.APM.Release,
		Debug:            true,
		Transport:        sentryHTTPSyncTransport,
		ServerName:       "internal-sentry",
		TracesSampleRate: 1.0,
		SampleRate:       1.0,
		AttachStacktrace: true,
	}); nil != err {
		logger.Fatal().Err(err).Msg("unable to initialize sentry client")
	}

	defer sentry.Flush(3 * time.Second)
	logger.Trace().Msg("successfully initialized sentry client")

	logger.Trace().Msg("initializing atr cache")
	atrCacheLogger := logger.With().Str("mod", "atr_cache").Logger()
	atrCache := cache.NewAtrCache(ctx, &atrCacheLogger, &redisClient)

	binanceLogger := logger.With().Str("mod", "binance").Logger()
	logger.Trace().Msg("initializing binance client")
	b := binance.New(&conf.Binance, binanceLogger, &redisClient)
	if err := b.SetInitialCandlesticks(sentry.SetHubOnContext(ctx, sentry.CurrentHub())); nil != err {
		logger.Fatal().Err(err).Msg("unable to set initial candlesticks")
	}
	logger.Trace().Msg("initial candlesticks data loaded into cache")

	var g errgroup.Group

	g.Go(func() error {
		defer binanceLogger.Trace().Msg("finished binance listener worker")
		hub := sentry.CurrentHub().Clone()
		ctx := sentry.SetHubOnContext(ctx, hub)
		ctx, cancel := context.WithCancel(ctx)
		go func() {
			logger.Trace().Msg("binding binance client listener to interrupt signal")
			c := make(chan os.Signal, 1)
			signal.Notify(c, os.Interrupt)
			logger.Trace().Msg("waiting for interrupt signal to stop binance listener")
			<-c
			logger.Info().Msg("stopping binance listener due to received interrupt signal")
			cancel()
		}()
		if err := b.Listen(ctx); nil != err {
			hub.CaptureException(err)
			return err
		}
		return nil
	})

	g.Go(func() error {
		hub := sentry.CurrentHub().Clone()
		ctx := sentry.SetHubOnContext(ctx, hub)
		logger := logger.With().Str("mod", "atr_worker").Logger()
		atrWorker := atr.NewWorker(&redisClient, &conf, &logger, atrCache)
		if err := atrWorker.Spawn(ctx); nil != err {
			hub.CaptureException(err)
			return err
		}
		return nil
	})

	serverLogger := logger.With().Str("mod", "server").Logger()
	server := http.NewServer(ctx, &conf.Server, &serverLogger, atrCache)

	g.Go(func() error {
		hub := sentry.CurrentHub().Clone()
		logger.Trace().Msg("binding server to interrupt signal")
		c := make(chan os.Signal, 1)
		signal.Notify(c, os.Interrupt)
		logger.Trace().Msg("waiting for interrupt signal to stop the server")
		<-c
		logger.Info().Msg("stopping server due to received interrupt signal")
		if err := server.Stop(); nil != err {
			hub.CaptureException(err)
			return err
		}
		return nil
	})

	logger.Info().Msg("starting server")
	if err := server.Listen(); nil != err {
		logger.Fatal().Err(err).Msg("unable to start server")
	}

	logger.Info().Msg("waiting for spawn workers to finish")
	if err := g.Wait(); nil != err {
		logger.Fatal().Err(err).Msg("unable to spawn worker threads")
	}
	logger.Trace().Msg("workers joined. exiting...")
}
