module gitlab.com/resistati/atr-provider

go 1.19

require (
	github.com/getsentry/sentry-go v0.13.0
	github.com/go-redis/redis/v8 v8.11.5
	github.com/gobwas/ws v1.1.0
	github.com/julienschmidt/httprouter v1.3.0
	github.com/rs/zerolog v1.28.0
	github.com/stretchr/testify v1.8.0
	github.com/tidwall/gjson v1.14.3
	golang.org/x/sync v0.0.0-20220929204114-8fcdb60fdcc0
	google.golang.org/protobuf v1.28.1
	gopkg.in/ini.v1 v1.67.0
)

require (
	github.com/cespare/xxhash/v2 v2.1.2 // indirect
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/dgryski/go-rendezvous v0.0.0-20200823014737-9f7001d12a5f // indirect
	github.com/gobwas/httphead v0.1.0 // indirect
	github.com/gobwas/pool v0.2.1 // indirect
	github.com/mattn/go-colorable v0.1.13 // indirect
	github.com/mattn/go-isatty v0.0.16 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	github.com/stretchr/objx v0.4.0 // indirect
	github.com/tidwall/match v1.1.1 // indirect
	github.com/tidwall/pretty v1.2.0 // indirect
	golang.org/x/sys v0.0.0-20220928140112-f11e5e49a4ec // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
