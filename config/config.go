package config

type ServerConfig struct {
	Host string
	Port uint16
}

type APMConfig struct {
	DSN     string
	Env     string
	Release string
}

type RedisConfig struct {
	Host     string
	Port     uint16
	Password string
}

type BinanceWebsocketConfig struct {
	Host string
}

type BinanceConfig struct {
	Websocket           BinanceWebsocketConfig
	CandlesticksPages   uint
	Symbol              string
	CandlestickInterval string
}

type Config struct {
	Server                        ServerConfig
	APM                           APMConfig
	Redis                         RedisConfig
	Binance                       BinanceConfig
	AtrRecalculateIntervalSeconds uint
	AtrCacheEvictionMinutes       uint
}
