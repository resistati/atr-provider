package config

import (
	"context"
	"strings"

	"github.com/rs/zerolog/log"
	"gopkg.in/ini.v1"
)

func Load(ctx context.Context) (Config, error) {
	logger := log.Ctx(ctx)

	cfg, err := ini.Load("config.ini")
	if nil != err {
		logger.Err(err).Msg("unable to load config file")
		return Config{}, err
	}
	logger.Trace().Msg("loaded config file")

	serverHost := cfg.Section(ini.DefaultSection).Key("server_host").String()
	logger.Debug().Str("value", serverHost).Msg("read server_host value from config file")

	serverPort, err := cfg.Section(ini.DefaultSection).Key("server_port").Uint()
	if nil != err {
		logger.Err(err).Msg("unable to read server_port value from config file")
		return Config{}, err
	}
	logger.Debug().Uint("value", serverPort).Msg("read server_port value from config file")

	atrRecalculateIntervalSeconds, err := cfg.Section(ini.DefaultSection).Key("atr_recalculate_interval_seconds").Uint()
	if nil != err {
		logger.Err(err).Msg("unable to read atr_recalculate_interval_seconds value from config file")
		return Config{}, err
	}
	logger.Debug().Uint("value", atrRecalculateIntervalSeconds).Msg("read atr_recalculate_interval_seconds value from config file")

	binanceCandlesticksPages, err := cfg.Section(ini.DefaultSection).Key("binance_candlesticks_number_of_pages").Uint()
	if nil != err {
		logger.Err(err).Msg("unable to read binance_candlesticks_number_of_pages value from config file")
		return Config{}, err
	}
	logger.Debug().Uint("value", binanceCandlesticksPages).Msg("read binance_candlesticks_number_of_pages value from config file")

	atrCacheEvictionMinutes, err := cfg.Section(ini.DefaultSection).Key("atr_cache_eviction_minutes").Uint()
	if nil != err {
		logger.Err(err).Msg("unable to read atr_cache_eviction_minutes value from config file")
		return Config{}, err
	}
	logger.Debug().Uint("value", atrCacheEvictionMinutes).Msg("read atr_cache_eviction_minutes value from config file")

	apmDsn := cfg.Section(ini.DefaultSection).Key("apm_dsn").String()
	if nil != err {
		logger.Err(err).Msg("unable to read apm_dsn value from config file")
		return Config{}, err
	}
	logger.Debug().Str("value", apmDsn).Msg("read apm_dsn value from config file")

	apmEnv := cfg.Section(ini.DefaultSection).Key("apm_env").String()
	if nil != err {
		logger.Err(err).Msg("unable to read apm_env value from config file")
		return Config{}, err
	}
	logger.Debug().Str("value", apmEnv).Msg("read apm_env value from config file")

	apmRelease := cfg.Section(ini.DefaultSection).Key("apm_release").String()
	if nil != err {
		logger.Err(err).Msg("unable to read apm_release value from config file")
		return Config{}, err
	}
	logger.Debug().Str("value", apmRelease).Msg("read apm_release value from config file")

	binanceSymbol := cfg.Section(ini.DefaultSection).Key("binance_symbol").String()
	if nil != err {
		logger.Err(err).Msg("unable to read binance_symbol value from config file")
		return Config{}, err
	}
	logger.Debug().Str("value", binanceSymbol).Msg("read binance_symbol value from config file")

	binanceWebsocketHost := cfg.Section(ini.DefaultSection).Key("binance_websocket_host").String()
	if nil != err {
		logger.Err(err).Msg("unable to read binance_websocket_host value from config file")
		return Config{}, err
	}
	logger.Debug().Str("value", binanceWebsocketHost).Msg("read binance_websocket_host value from config file")

	candlestickInterval := cfg.Section(ini.DefaultSection).Key("candlestick_interval").String()
	if nil != err {
		logger.Err(err).Msg("unable to read candlestick_interval value from config file")
		return Config{}, err
	}
	logger.Debug().Str("value", candlestickInterval).Msg("read candlestick_interval value from config file")

	redisHost := cfg.Section(ini.DefaultSection).Key("redis_host").String()
	if nil != err {
		logger.Err(err).Msg("unable to read redis_host value from config file")
		return Config{}, err
	}
	logger.Debug().Str("value", redisHost).Msg("read redis_host value from config file")

	redisPort, err := cfg.Section(ini.DefaultSection).Key("redis_port").Uint()
	if nil != err {
		logger.Err(err).Msg("unable to read redis_port value from config file")
		return Config{}, err
	}
	logger.Debug().Uint("value", redisPort).Msg("read redis_port value from config file")

	redisPasswd := cfg.Section(ini.DefaultSection).Key("redis_passwd").String()
	if nil != err {
		logger.Err(err).Msg("unable to read redis_passwd value from config file")
		return Config{}, err
	}
	logger.Debug().Str("value", strings.Repeat("*", len(redisPasswd))).Msg("read redis_passwd value from config file")

	return Config{
		Server: ServerConfig{
			Host: serverHost,
			Port: uint16(serverPort),
		},
		APM: APMConfig{
			DSN:     apmDsn,
			Env:     apmEnv,
			Release: apmRelease,
		},
		Redis: RedisConfig{
			Host:     redisHost,
			Port:     uint16(redisPort),
			Password: redisPasswd,
		},
		AtrRecalculateIntervalSeconds: atrRecalculateIntervalSeconds,
		Binance: BinanceConfig{
			CandlesticksPages:   binanceCandlesticksPages,
			Symbol:              binanceSymbol,
			CandlestickInterval: candlestickInterval,
			Websocket: BinanceWebsocketConfig{
				Host: binanceWebsocketHost,
			},
		},
		AtrCacheEvictionMinutes: atrCacheEvictionMinutes,
	}, nil
}
