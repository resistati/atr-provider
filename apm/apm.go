package apm

type APMContextKey string

const (
	SentrySpanStatusContextKey APMContextKey = "sentry-span-status"
	SentryChildSpanContextKey  APMContextKey = "sentry-child-span"
)
