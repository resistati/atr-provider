FROM docker.io/library/golang:1 AS build

RUN apt-get update && apt-get upgrade -y

RUN apt-get install -y libssl-dev pkg-config

RUN update-ca-certificates

RUN useradd --create-home --uid 1000 nonroot

USER nonroot

WORKDIR /home/nonroot/atr-provider

COPY go.sum go.mod ./

RUN go mod download -x

COPY . .

RUN go build

FROM gcr.io/distroless/base-debian11:latest

USER nonroot:nonroot

COPY --from=build /home/nonroot/atr-provider/atr-provider /atr-provider

ENTRYPOINT [ "/atr-provider" ]
