package http

import (
	"context"
	"errors"
	"fmt"
	"net/http"

	"github.com/getsentry/sentry-go"
	"github.com/julienschmidt/httprouter"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"

	"gitlab.com/resistati/atr-provider/cache"
	"gitlab.com/resistati/atr-provider/config"
)

type Server struct {
	ctx       context.Context
	router    *httprouter.Router
	logger    *zerolog.Logger
	atrGetter cache.AtrGetter
	internal  *http.Server
}

func NewServer(
	ctx context.Context,
	conf *config.ServerConfig,
	logger *zerolog.Logger,
	atrGetter cache.AtrGetter,
) Server {
	router := httprouter.New()
	server := Server{
		ctx:       ctx,
		router:    router,
		atrGetter: atrGetter,
		logger:    logger,
		internal: &http.Server{
			Addr:    fmt.Sprintf("%s:%d", conf.Host, conf.Port),
			Handler: router,
		},
	}
	router.GET("/health/ping", server.handlePing)
	router.GET("/atr", server.handleGetAtr)
	return server
}

func (s *Server) Listen() error {
	logger := log.Ctx(s.ctx)

	logger.Info().Str("address", s.internal.Addr).Msg("starting http server")
	if err := s.internal.ListenAndServe(); nil != err {
		if errors.Is(err, http.ErrServerClosed) {
			logger.Trace().Msg("server closed")
			return nil
		}
		logger.Err(err).Msg("unable to start http server")

		return err
	}

	return nil
}

func (s *Server) Stop() error {
	log.Ctx(s.ctx).Trace().Msg("shutting server down")
	return s.internal.Shutdown(s.ctx)
}

func (s *Server) handlePing(res http.ResponseWriter, req *http.Request, _ httprouter.Params) {
	logger := s.logger.With().Str("api", "ping").Logger()
	logger.Trace().Msg("handling request")

	hub := sentry.CurrentHub().Clone()
	ctx := sentry.SetHubOnContext(req.Context(), hub)
	span := sentry.StartSpan(ctx, "handle-ping", sentry.TransactionName("handle-ping"))

	res.Header().Add("Content-Type", "text/plain")
	logger.Trace().Msg("written response header")

	child := span.StartChild("write-response")
	defer func() {
		span.Status = child.Status
		span.Finish()
	}()

	n, err := fmt.Fprint(res, "pong")
	if nil != err {
		defer child.Finish()
		logger.Err(err).Msg("unable to write response")
		child.Status = sentry.SpanStatusInternalError
		hub.CaptureException(err)
		return
	}
	logger.Trace().Int("bytes", n).Msg("written response")
	child.Status = sentry.SpanStatusOK
	child.Finish()
}

func (s *Server) handleGetAtr(res http.ResponseWriter, req *http.Request, _ httprouter.Params) {
	logger := s.logger.With().Str("api", "get-atr").Logger()
	logger.Trace().Msg("handling request")

	hub := sentry.CurrentHub().Clone()
	ctx := sentry.SetHubOnContext(req.Context(), hub)
	span := sentry.StartSpan(ctx, "get-atr", sentry.TransactionName("handle-get-atr"))
	defer span.Finish()

	res.Header().Add("Content-Type", "text/plain")

	atr, err := s.atrGetter.Get(span.Context())
	if nil != err {
		if errors.Is(err, cache.ErrNotSet) {
			logger.Warn().Msg("atr cache is not set. responding with status 425")

			res.WriteHeader(http.StatusTooEarly)
			logger.Trace().Msg("written response header")

			n, err := fmt.Fprint(res, "try seconds later")
			if nil != err {
				logger.Err(err).Msg("unable to write response")
				span.Status = sentry.SpanStatusInternalError
				hub.CaptureException(err)
				return
			}
			logger.Trace().Int("bytes", n).Msg("written response")
			return
		}
		logger.Err(err).Msg("unable to read atr cache. responding with status 500")
		hub.CaptureException(err)

		res.WriteHeader(http.StatusInternalServerError)
		logger.Trace().Msg("written response status header")

		n, err := fmt.Fprint(res, err.Error())
		if nil != err {
			logger.Err(err).Msg("unable to write response")
			span.Status = sentry.SpanStatusInternalError
			hub.CaptureException(err)
			return
		}
		logger.Trace().Int("bytes", n).Msg("written response")
		return
	}
	logger.Trace().Msg("read atr from cache")

	child := span.StartChild("write-response")
	res.WriteHeader(http.StatusOK)
	logger.Trace().Msg("written response status header")

	n, err := fmt.Fprint(res, atr)
	if nil != err {
		defer child.Finish()
		logger.Err(err).Msg("unable to write response")
		child.Status = sentry.SpanStatusInternalError
		hub.CaptureException(err)
		return
	}
	logger.Trace().Int("bytes", n).Msg("written response")
	child.Status = sentry.SpanStatusOK
	child.Finish()
}
