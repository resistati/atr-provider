package binance

import (
	"context"
	"errors"
	"fmt"
	"io"
	"net"
	"net/http"
	"net/url"
	"strings"
	"time"

	"github.com/getsentry/sentry-go"
	"github.com/gobwas/ws"
	"github.com/gobwas/ws/wsutil"
	"github.com/rs/zerolog"
	"github.com/tidwall/gjson"

	"gitlab.com/resistati/atr-provider/candlestick"
	"gitlab.com/resistati/atr-provider/config"
	"gitlab.com/resistati/atr-provider/errdonegroup"
	"gitlab.com/resistati/atr-provider/redis"
)

type BinanceWorker struct {
	config *config.BinanceConfig
	logger zerolog.Logger
	redis  *redis.RedisClient
}

func New(
	config *config.BinanceConfig,
	logger zerolog.Logger,
	redis *redis.RedisClient,
) BinanceWorker {
	return BinanceWorker{config, logger, redis}
}

func (b *BinanceWorker) Listen(ctx context.Context) error {
	hub := sentry.GetHubFromContext(ctx)
	connectionUrl := fmt.Sprintf(
		"wss://%s/ws/%s@kline_%s",
		b.config.Websocket.Host,
		b.config.Symbol,
		b.config.CandlestickInterval,
	)
	b.logger.Debug().Str("url", connectionUrl).Msg("connecting to binance websocket stream")
	conn, reader, hs, err := ws.Dial(ctx, connectionUrl)
	if nil != err {
		b.logger.Err(err).Msg("unable to connect to binance websocket stream")
		hub.CaptureException(err)
		return err
	}
	b.logger.Debug().Str("protocol", hs.Protocol).Msg("connected to binance websocket stream")

	if nil != reader {
		b.logger.Warn().Msg("reading binance websocket initial buffer as it is not empty")
		bs, err := io.ReadAll(reader)
		if nil != err {
			b.logger.Err(err).Msg("unable to read binance websocket initial buffer")
			hub.CaptureException(err)
			return err
		}
		b.logger.Debug().Str("content", string(bs)).Msg("read binance websocket initial buffer")
	}

	g := errdonegroup.New(ctx)

	g.Go(func(done <-chan struct{}) error {
		hub := sentry.CurrentHub().Clone()
		ctx := sentry.SetHubOnContext(ctx, hub)
		var interval time.Duration = 30 * time.Second
		ticker := time.NewTicker(interval)
		b.logger.Debug().Dur("interval", interval).Msg("initialized binance pong replier ticker")
		defer func() {
			ticker.Stop()
			b.logger.Trace().Msg("stopped pong replier ticker")
		}()

		for {
			select {
			case <-done:
				b.logger.Trace().Msg("stopping pong replier")
				return nil
			case <-ticker.C:
				span := sentry.StartSpan(ctx, "pong-binance", sentry.TransactionName("reply-pong-binance"))
				if err := wsutil.WriteClientMessage(conn, ws.OpPong, []byte{}); err != nil {
					defer span.Finish()
					b.logger.Err(err).Msg("unable to send pong message to binance")
					hub.CaptureException(err)
					return err
				}
				b.logger.Trace().Msg("sent pong message to binance")
				span.Finish()
			}
		}
	})

	g.Go(func(done <-chan struct{}) error {
		defer b.logger.Trace().Msg("finished binance stream listener loop")

		hub := sentry.CurrentHub().Clone()
		ctx := sentry.SetHubOnContext(ctx, hub)

		go func(hub *sentry.Hub) {
			<-done
			if err := conn.Close(); nil != err {
				b.logger.Err(err).Msg("unable to stop binance stream listener")
				hub.CaptureException(err)
				return
			}
			b.logger.Trace().Msg("closed websocket connection to binance")
		}(hub.Clone())

		for {
			b.logger.Trace().Msg("waiting for next frame from binance")

			header, err := ws.ReadHeader(conn)
			if nil != err {
				if errors.Is(err, net.ErrClosed) {
					b.logger.Trace().Msg("finishing binance stream listener without any errors as it is an expected error case")
					return nil
				}
				b.logger.Err(err).Msg("unable to read binance frame header")
				hub.CaptureException(err)
				return err
			}
			b.logger.Trace().Msg("read frame header")

			span := sentry.StartSpan(ctx, "get-binance-frame", sentry.TransactionName("get-binance-frame"))
			defer span.Finish()

			child := span.StartChild("process-opcode")
			if opCode := header.OpCode; opCode == ws.OpClose {
				b.logger.Debug().Msg("received close op-code from binance")
				child.Finish()
				return nil
			} else if opCode == ws.OpPing {
				b.logger.Debug().Msg("received ping op-code from binance")
				child.Finish()
				span.Finish()
				continue
			}
			child.Finish()

			child = span.StartChild("read-payload")
			payload := make([]byte, header.Length)
			if _, err = io.ReadFull(conn, payload); err != nil {
				defer child.Finish()
				b.logger.Err(err).Msg("unable to read binance frame payload")
				hub.CaptureException(err)
				return err
			}
			b.logger.Debug().Bytes("payload", payload).Msg("read binance frame payload")
			child.Finish()

			if header.Masked {
				child = span.StartChild("unmask-payload")
				ws.Cipher(payload, header.Mask, 0)
				b.logger.Debug().Msg("unmasked binance frame payload")
				child.Finish()
			}

			child = span.StartChild("stringify-payload")
			var builder strings.Builder
			if _, err := builder.Write(payload); nil != err {
				defer child.Finish()
				b.logger.Err(err).Msg("unable to convert binance frame payload to string")
				hub.CaptureException(err)
				return err
			}
			b.logger.Trace().Msg("converted binance frame payload to string")
			child.Finish()

			child = span.StartChild("parse-payload")
			binanceCandlestick, err := parseBinanceCandlestick(payload)
			if nil != err {
				defer child.Finish()
				b.logger.Err(err).Msg("unable to parse binance frame payload to candlestick type")
				hub.CaptureException(err)
				return err
			}
			b.logger.Debug().Stringer("candlestick", binanceCandlestick).Msg("converted binance frame payload to candlestick type")
			child.Finish()

			child = span.StartChild("get-last-cached-candlestick")
			lastCachedCandlestick, err := b.redis.GetLast(ctx)
			if nil != err {
				defer child.Finish()
				b.logger.Err(err).Msg("unable to get last cached candlestick")
				hub.CaptureException(err)
				return err
			}
			b.logger.Debug().Stringer("candlestick", lastCachedCandlestick).Msg("got last cached candlestick")
			child.Finish()

			if binanceCandlestick.OpenTime == lastCachedCandlestick.CloseTime+1 {
				child = span.StartChild("append-candlestick-to-cache")
				if err := b.redis.Append(ctx, binanceCandlestick); nil != err {
					defer child.Finish()
					b.logger.Err(err).Msg("unable to append binance candlestick to cache")
					hub.CaptureException(err)
					return err
				}
				b.logger.Trace().Msg("appended binance candlestick to cache")
				child.Finish()
			} else if binanceCandlestick.OpenTime == lastCachedCandlestick.OpenTime {
				child = span.StartChild("replace-candlestick-in-cache")
				if err := b.redis.UpdateLast(ctx, binanceCandlestick); nil != err {
					defer child.Finish()
					b.logger.Err(err).Msg("unable to replace binance candlestick with last item in cache")
					hub.CaptureException(err)
					return err
				}
				b.logger.Trace().Msg("replaced binance candlestick with last item in cache")
				child.Finish()
			} else {
				hub.CaptureException(errors.New("missing candlestick in stream"))
				panic("missing candlestick in stream")
			}
			span.Finish()
		}
	})

	return g.Wait()
}

func (b *BinanceWorker) SetInitialCandlesticks(ctx context.Context) error {
	hub := sentry.CurrentHub()
	ctx = sentry.SetHubOnContext(ctx, hub)
	span := sentry.StartSpan(ctx, "set-initial-candlesticks", sentry.TransactionName("set-initial-candlesticks"))
	defer span.Finish()
	child := span.StartChild("reset-redis")
	if err := b.redis.Reset(ctx); nil != err {
		defer child.Finish()
		b.logger.Err(err).Msg("unable to reset redis cache")
		hub.CaptureException(err)
		return err
	}
	b.logger.Trace().Msg("reset redis cache")
	child.Finish()

	candlesticks, err := b.getCandlesticks(
		span.Context(),
		b.config.CandlesticksPages,
		b.config.Symbol,
		b.config.CandlestickInterval,
	)
	if nil != err {
		defer child.Finish()
		b.logger.Err(err).Msg("unable to get initial candlesticks data from binance api")
		hub.CaptureException(err)
		return err
	}
	b.logger.Debug().Int("number_of_items", len(candlesticks)).Msg("got initial candlesticks data from binance api")
	child.Finish()

	child = span.StartChild("save-candlesticks-in-redis")
	if err := b.redis.SetInitialCandlesticks(ctx, candlesticks); nil != err {
		defer child.Finish()
		b.logger.Err(err).Msg("unable to set initial candlesticks data to cache")
		hub.CaptureException(err)
		return err
	}
	b.logger.Trace().Msg("set initial candlesticks data to cache")
	child.Finish()

	return nil
}

const Pages uint = 1

func (b *BinanceWorker) getCandlesticks(ctx context.Context, pages uint, symbol, interval string) ([]candlestick.Candlestick, error) {
	hub := sentry.GetHubFromContext(ctx)
	span := sentry.StartSpan(ctx, "get-candlesticks-from-binance")
	defer span.Finish()

	out := []candlestick.Candlestick{}

	limit := "1500"
	values := url.Values{}
	values.Add("limit", limit)
	values.Add("symbol", symbol)
	values.Add("interval", interval)

	for i := uint(0); i < pages; i++ {
		pageSpan := span.StartChild(fmt.Sprintf("get_page_%d", i))
		child := pageSpan.StartChild("fetch_page")
		logger := b.logger.With().Uint("page", i).Logger()
		resp, err := http.Get(fmt.Sprintf("https://fapi.binance.com/fapi/v1/klines?%s", values.Encode()))
		if nil != err {
			defer child.Finish()
			logger.Err(err).Msg("unable to get candlesticks data from Binance")
			child.Status = sentry.SpanStatusInternalError
			hub.CaptureException(err)
			return nil, err
		}
		logger.Trace().Msg("received response from Binance")
		child.Status = sentry.SpanStatusOK
		child.Finish()

		child = pageSpan.StartChild("read_binance_response")
		bytes, err := io.ReadAll(resp.Body)
		if nil != err {
			defer child.Finish()
			logger.Err(err).Msg("unable to read Binance response body")
			child.Status = sentry.SpanStatusInternalError
			hub.CaptureException(err)
			return nil, err
		}
		logger.Trace().Int("bytes", len(bytes)).Msg("read Binance response body")
		child.Status = sentry.SpanStatusOK
		child.Finish()

		child = pageSpan.StartChild("close_binance_response_body")
		if err := resp.Body.Close(); nil != err {
			defer child.Finish()
			logger.Err(err).Msg("unable to close Binance response body")
			child.Status = sentry.SpanStatusInternalError
			hub.CaptureException(err)
			return nil, err
		}
		logger.Trace().Msg("closed Binance response body")
		child.Status = sentry.SpanStatusOK
		child.Finish()

		if statusCode := resp.StatusCode; statusCode != http.StatusOK {
			logger.Error().Err(err).Bytes("response_body", bytes).Msgf("received status code %d from Binance", statusCode)
			return out, nil
		}

		child = pageSpan.StartChild("parse_binance_response")
		items := gjson.ParseBytes(bytes).Array()
		child.Status = sentry.SpanStatusOK
		child.Finish()
		if len(items) == 0 {
			logger.Trace().Msg("reached end of candlesticks data")
			pageSpan.Finish()
			break
		}
		logger.
			Debug().
			Int("number of candlesticks in page", len(items)).
			Msg("parsed candlesticks json response")

		child = pageSpan.StartChild("parse_candlesticks_data")
		candlesticks := candlestick.FromBinanceResponse(items)
		logger.Trace().Msg("parsed raw candlesticks data")
		child.Status = sentry.SpanStatusOK
		child.Finish()

		endTime := fmt.Sprintf("%d", candlesticks[0].OpenTime-1)
		values.Set("endTime", endTime)
		logger.Debug().Str("previous page end time", endTime).Msg("using new end time for previous page")

		child = pageSpan.StartChild("resize_output_slice")
		temp := make([]candlestick.Candlestick, len(candlesticks)+len(out))
		copy(temp, candlesticks)
		copy(temp[len(candlesticks):], out)
		out = temp
		child.Status = sentry.SpanStatusOK
		child.Finish()
		pageSpan.Finish()
	}

	return out, nil
}
