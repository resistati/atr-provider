package binance

import (
	"strconv"

	"github.com/tidwall/gjson"

	"gitlab.com/resistati/atr-provider/candlestick"
)

func parseBinanceCandlestick(in []byte) (candlestick.Candlestick, error) {
	o := gjson.ParseBytes(in).Get("k")

	openTime, err := strconv.ParseInt(o.Get("t").Raw, 10, 64)
	if nil != err {
		return candlestick.Candlestick{}, err
	}

	closeTime, err := strconv.ParseInt(o.Get("T").Raw, 10, 64)
	if nil != err {
		return candlestick.Candlestick{}, err
	}

	open, err := strconv.ParseFloat(o.Get("o").String(), 64)
	if nil != err {
		return candlestick.Candlestick{}, err
	}

	close, err := strconv.ParseFloat(o.Get("c").String(), 64)
	if nil != err {
		return candlestick.Candlestick{}, err
	}

	high, err := strconv.ParseFloat(o.Get("h").String(), 64)
	if nil != err {
		return candlestick.Candlestick{}, err
	}

	low, err := strconv.ParseFloat(o.Get("l").String(), 64)
	if nil != err {
		return candlestick.Candlestick{}, err
	}

	c := candlestick.Candlestick{
		OpenTime:  openTime,
		Open:      open,
		Close:     close,
		High:      high,
		Low:       low,
		CloseTime: closeTime,
	}

	return c, nil
}
