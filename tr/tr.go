package tr

import (
	"math"

	"gitlab.com/resistati/atr-provider/candlestick"
)

func FromCandlesticks(candlesticks []candlestick.Candlestick) []float64 {
	trs := make([]float64, len(candlesticks))
	for bar := len(candlesticks) - 1; bar > 0; bar-- {
		trs[bar] = math.Max(
			math.Max(
				candlesticks[bar].High-candlesticks[bar].Low,
				math.Abs(candlesticks[bar].High-candlesticks[bar-1].Close),
			),
			math.Abs(candlesticks[bar].Low-candlesticks[bar-1].Close),
		)
	}

	trs[0] = candlesticks[0].High - candlesticks[0].Low

	return trs
}
