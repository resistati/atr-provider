package rma

import (
	"fmt"
)

type PeriodLength uint

const (
	PeriodLength5   PeriodLength = 5
	PeriodLength11  PeriodLength = 11
	PeriodLength22  PeriodLength = 22
	PeriodLength45  PeriodLength = 45
	PeriodLength91  PeriodLength = 91
	PeriodLength182 PeriodLength = 182
	PeriodLength364 PeriodLength = 364
)

func periodLengthToTrueRangeMinSize(periodLength PeriodLength) int {
	size, exists := map[PeriodLength]int{
		PeriodLength5:   250,
		PeriodLength11:  350,
		PeriodLength22:  650,
		PeriodLength45:  1100,
		PeriodLength91:  2200,
		PeriodLength182: 4200,
		PeriodLength364: 7500,
	}[periodLength]

	if !exists {
		panic(fmt.Sprintf("period length %d is not a valid supported period length", periodLength))
	}

	return size
}

func Rma(src []float64, length PeriodLength) float64 {
	alpha := 1.0 / float64(length)

	return rec(src, alpha, periodLengthToTrueRangeMinSize(length))
}

func rec(src []float64, alpha float64, max int) float64 {
	if max == 0 {
		return src[len(src)-1]
	}

	return src[len(src)-1]*alpha + (1-alpha)*rec(src[:len(src)-1], alpha, max-1)
}
