package rma_test

import (
	"encoding/json"
	"io/ioutil"
	"math"
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/resistati/atr-provider/rma"
)

func readTestDataset() []float64 {
	fileContents, err := ioutil.ReadFile("test.json")
	if nil != err {
		panic(err)
	}

	dataset := make([]float64, 0, 6*1500)
	if err := json.Unmarshal(fileContents, &dataset); nil != err {
		panic(err)
	}

	return dataset
}

func Test_RmaForPeriodLength5(t *testing.T) {
	trueRanges := readTestDataset()
	result := rma.Rma(trueRanges, rma.PeriodLength5)
	assert.InDelta(t, 1.3951534622558677, result, math.Pow10(-10))
}

func Test_RmaForPeriodLength11(t *testing.T) {
	trueRanges := readTestDataset()
	result := rma.Rma(trueRanges, rma.PeriodLength11)
	assert.InDelta(t, 1.822585620123842, result, math.Pow10(-10))
}

func Test_RmaForPeriodLength22(t *testing.T) {
	trueRanges := readTestDataset()
	result := rma.Rma(trueRanges, rma.PeriodLength22)
	assert.InDelta(t, 2.1075042475837082, result, math.Pow10(-10))
}

func Test_RmaForPeriodLength45(t *testing.T) {
	trueRanges := readTestDataset()
	result := rma.Rma(trueRanges, rma.PeriodLength45)
	assert.InDelta(t, 2.213808302322358, result, math.Pow10(-10))
}

func Test_RmaForPeriodLength91(t *testing.T) {
	trueRanges := readTestDataset()
	result := rma.Rma(trueRanges, rma.PeriodLength91)
	assert.InDelta(t, 2.0839581309314195, result, math.Pow10(-10))
}

func Test_RmaForPeriodLength182(t *testing.T) {
	trueRanges := readTestDataset()
	result := rma.Rma(trueRanges, rma.PeriodLength182)
	assert.InDelta(t, 1.7889069964871152, result, math.Pow10(-10))
}

func Test_RmaForPeriodLength364(t *testing.T) {
	trueRanges := readTestDataset()
	result := rma.Rma(trueRanges, rma.PeriodLength364)
	assert.InDelta(t, 1.560029517190176, result, math.Pow10(-10))
}
