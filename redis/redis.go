package redis

import (
	"bytes"
	"context"
	"fmt"
	"strings"

	"github.com/go-redis/redis/v8"
	"google.golang.org/protobuf/proto"

	"gitlab.com/resistati/atr-provider/candlestick"
	"gitlab.com/resistati/atr-provider/config"
)

const candlesticksRedisKey string = "candlesticks"
const atrRedisKey string = "atr"

type RedisClient struct {
	client *redis.Client
	conf   *config.RedisConfig
}

func New(conf *config.RedisConfig) RedisClient {
	return RedisClient{nil, conf}
}

func (r *RedisClient) Connect(ctx context.Context) error {
	rdb := redis.NewClient(&redis.Options{
		Addr:     fmt.Sprintf("%s:%d", r.conf.Host, r.conf.Port),
		Password: r.conf.Password,
		DB:       0,
	})
	if err := rdb.Ping(ctx).Err(); nil != err {
		return err
	}

	r.client = rdb

	return nil
}

func (r *RedisClient) Close() error {
	return r.client.Close()
}

func (r *RedisClient) Reset(ctx context.Context) error {
	return r.client.FlushAll(ctx).Err()
}

func (r *RedisClient) GetAtr(ctx context.Context) (float64, error) {
	return r.client.Get(ctx, atrRedisKey).Float64()
}

func (r *RedisClient) SetAtr(ctx context.Context, atr float64) error {
	return r.client.Set(ctx, atrRedisKey, atr, 0).Err()
}

func (r *RedisClient) GetLast(ctx context.Context) (candlestick.Candlestick, error) {
	bs, err := r.client.LIndex(ctx, candlesticksRedisKey, -1).Bytes()
	if nil != err {
		return candlestick.Candlestick{}, err
	}

	return deserializeCandlestick(bs)
}

func (r *RedisClient) Append(ctx context.Context, cs candlestick.Candlestick) error {
	serializedCandlestick, err := serializeCandlestick(cs)
	if nil != err {
		return err
	}

	return r.client.RPush(ctx, candlesticksRedisKey, serializedCandlestick).Err()
}

func (r *RedisClient) UpdateLast(ctx context.Context, cs candlestick.Candlestick) error {
	serializedCandlestick, err := serializeCandlestick(cs)
	if nil != err {
		return err
	}

	return r.client.LSet(ctx, candlesticksRedisKey, -1, serializedCandlestick).Err()
}

func (r *RedisClient) SetInitialCandlesticks(ctx context.Context, candleSticks []candlestick.Candlestick) error {
	serializedCandlesticks := make([]string, len(candleSticks))
	for i, cs := range candleSticks {
		serializedCandlestick, err := serializeCandlestick(cs)
		if nil != err {
			return err
		}
		serializedCandlesticks[i] = serializedCandlestick
	}
	if err := r.client.RPush(ctx, candlesticksRedisKey, serializedCandlesticks).Err(); nil != err {
		return err
	}

	return nil
}

func (r *RedisClient) GetCandlesticks(ctx context.Context) ([]candlestick.Candlestick, error) {
	cmd := r.client.LRange(ctx, candlesticksRedisKey, 0, -1)
	if err := cmd.Err(); nil != err {
		return nil, err
	}
	stringsResult, err := cmd.Result()
	if nil != err {
		return nil, err
	}

	out := make([]candlestick.Candlestick, len(stringsResult))
	for i, s := range stringsResult {
		cs, err := deserializeCandlestick(bytes.NewBufferString(s).Bytes())
		if nil != err {
			return nil, err
		}

		out[i] = cs
	}

	return out, nil
}

func serializeCandlestick(cs candlestick.Candlestick) (string, error) {
	var builder strings.Builder
	x := candlestick.CandlestickPb{
		OpenTime:  cs.OpenTime,
		CloseTime: cs.CloseTime,
		Open:      cs.Open,
		Close:     cs.Close,
		High:      cs.High,
		Low:       cs.Low,
	}
	b, err := proto.Marshal(&x)
	if nil != err {
		return "", err
	}
	if _, err := builder.Write(b); nil != err {
		return "", err
	}

	return builder.String(), nil
}

func deserializeCandlestick(raw []byte) (candlestick.Candlestick, error) {
	var buffer bytes.Buffer
	if _, err := buffer.Write(raw); nil != err {
		return candlestick.Candlestick{}, err
	}
	pb := candlestick.CandlestickPb{}
	if err := proto.Unmarshal(buffer.Bytes(), &pb); nil != err {
		return candlestick.Candlestick{}, err
	}
	buffer.Reset()
	return candlestick.Candlestick{
		OpenTime:  pb.OpenTime,
		Open:      pb.Open,
		Close:     pb.Close,
		High:      pb.High,
		Low:       pb.Low,
		CloseTime: pb.CloseTime,
	}, nil
}
