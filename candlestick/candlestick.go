package candlestick

import (
	"fmt"

	"github.com/tidwall/gjson"
)

type Candlestick struct {
	OpenTime  int64
	Open      float64
	Close     float64
	CloseTime int64
	High      float64
	Low       float64
}

func (c Candlestick) String() string {
	return fmt.Sprintf("%#+v", c)
}

func FromBinanceResponse(items []gjson.Result) []Candlestick {
	candlesticks := make([]Candlestick, len(items))

	for i, item := range items {
		candlestick := item.Array()
		candlesticks[i] = Candlestick{
			OpenTime:  candlestick[0].Int(),
			Open:      candlestick[1].Float(),
			High:      candlestick[2].Float(),
			Low:       candlestick[3].Float(),
			Close:     candlestick[4].Float(),
			CloseTime: candlestick[6].Int(),
		}
	}

	return candlesticks
}
