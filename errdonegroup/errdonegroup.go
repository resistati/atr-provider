package errdonegroup

import (
	"context"

	"golang.org/x/sync/errgroup"
)

type ErrDoneGroup struct {
	parent  context.Context
	eg      errgroup.Group
	members uint
	done    chan struct{}
}

func New(ctx context.Context) ErrDoneGroup {
	return ErrDoneGroup{ctx, errgroup.Group{}, 0, make(chan struct{})}
}

func (cdg *ErrDoneGroup) Go(fn func(<-chan struct{}) error) {
	cdg.members++
	cdg.eg.Go(func() error {
		defer func() {
			cdg.members--
		}()
		return fn(cdg.done)
	})
}

func (cdg *ErrDoneGroup) Wait() error {
	<-cdg.parent.Done()
	allMembers := cdg.members
	for i := uint(0); i < allMembers; i++ {
		cdg.done <- struct{}{}
	}
	return cdg.eg.Wait()
}
